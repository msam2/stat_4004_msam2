---
title: 'Statistics 4004: Homework 7'
output:
  word_document: default
  pdf_document: default
date: "`r Sys.Date()`"
subtitle: Due Tuesday Apr 16, start of class
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(data.table)
library(tidyverse)
library(yaml)
library(knitr)
devtools::install_github("rstudio/rmarkdown")
```

## Problem 1


## Problem 2 [4 points]

1. determine the number of centroids to use
2. using the kmeans function, find the optimum centroids on the training set
3. repeat (2) 50 times (is this a parameter)
4. what would be a good measure of "cost"?
5. given a good measure of cost, choose the optimum set
6. given optimum centroids, classify the centroids using knn, what value for k?
7. what do the labels correspond to?  are there duplicate labels? can you view a couple with duplicate labels, do they look like the should have clustered separately?

#I've commented out most of this

```{r code1}
library(ElemStatLearn)
data(zip.train)
dim(zip.train) # 7291 training observations. 
dim(zip.test) # 2007 testing observations. 
# each row:  starts with class labels: 0-9 followed by 16X16 grayscale images as a vector.

# plot one image.
#im1 = zip2image(zip.train, 1) # transform the first row into image format.
#image(im1, col=gray(256:0/256), xlab="", ylab="" ) 

#zip.train[1,1] # this is the label of this image.

# extract the features and the labels.
train.X = zip.train[, 2:257]
train.Y = zip.train[,1]
test.X = zip.test[,2:257]
test.Y = zip.test[,1]
table(train.Y)

# Now apply KNN using knn() in 'FNN' package. 
library('FNN') # load in the package FNN.
#?knn
#ptm <- proc.time() # Start the clock.
#FNN::functionname() is to specify that we are using the function from FNN package.
#result = FNN::knn(train.X, test.X, train.Y, k=4, prob=TRUE)
#time3=proc.time() - ptm # Stop the clock

# Look at result and evaluate the misclassification rate.
#str(result)
#result[1:10] # the first 10 predicted labels. 
#View(data.frame(result, as.factor(test.Y)))

#Tab = table(result, as.factor(test.Y))
#misRate = 1- sum(diag(Tab))/length(test.Y) 
#misRate # 0.0568

# Let's plot those that were misclassified. 
#mis.index = which(as.character(result)!=as.character(test.Y))
#for (i in 1:10){
#  id = mis.index[i] # the index in test set that corresponds to misclassified image.
#  im.i = zip2image(zip.test, id)
#  image(im.i, col=gray(256:0/256), xlab="", ylab="", 
#        main=paste('i=',id,'; True=',test.Y[id], '; Pred=', as.character(result[id])))
#  scan() # wait for a  keyboard hit to continue. 
#}

```

```{r code2}
library(factoextra)
set.seed(123)
km.res <- kmeans(train.X, 10, nstart = 25)
fviz_cluster(km.res, train.X, frame = FALSE, geom = "point")
```




```{r code3}
#Elbow Method for finding the optimal number of clusters
set.seed(123)
# Compute and plot wss for k = 2 to k = 15.
k.max <- 15
wss <- sapply(1:k.max, 
              function(k){kmeans(train.X, k, nstart=25,iter.max = 50 )$tot.withinss})
wss
plot(1:k.max, wss,
     type="b", pch = 19, frame = FALSE, 
     xlab="Number of clusters K",
     ylab="Total within-clusters sum of squares")
```
#I tried to do the elbow method here with 50 iterations and I was surprised to find that the elbow appears to be at 6 or 7 clusters. I was under the impression that it should be at 10 so I'm not sure what to do with this information. There's certainly more than 6 numbers that we are looking at.


## Problem 3 [4 points]


```{r code4}
set.seed(123)
N  <- 100
X1 <- rnorm(N, 175, 7)
X2 <- rnorm(N,  30, 8)
X3 <- abs(rnorm(N, 60, 30))
#I'm assigning the betas because it's less confusing for me
beta0 = 10
beta1 = 0.5
beta2 = -0.3
beta3 = -0.4
Y  <- beta1*X1 + beta2*X2 + beta3*X3 + beta0 + rnorm(N, 0, 3)
dfRegr <- data.frame(X1, X2, X3, Y)

```

1. For bootstrapping, use 100 bootstraps.
2. What are the parameters you obtain estimates for?
3. Create a table output of the estimates using both methods.

```{r}
beta_hat_ALL <- lm(Y~X1+X2+X3)$coefficients
beta_hat_ALL
beta_hat_BOOT = matrix(NA, nrow=N, ncol=4)

for (i in 1:N){
  resample.idx = sample(1:100, size=100, replace=TRUE) 
  beta_hat_BOOT[i,] <- lm(Y[resample.idx] ~ X1[resample.idx] +X2[resample.idx] +X3[resample.idx])$coefficients
}


# the histogram of bootstrap distribution.
par(mfcol=c(2,3))
#I'm plugging in the true values as found by beta_hat_ALL

hist(beta_hat_BOOT[,1], breaks=20, freq=FALSE, main="beta0", xlab=NA)
abline(v=13.921855,col="red")
abline(v=quantile(beta_hat_BOOT[,1], prob=c(0.025, 0.975)), col="blue")
hist(beta_hat_BOOT[,2], breaks=20, freq=FALSE, main="beta1",xlab=NA)
abline(v=0.4762354,col="red")
abline(v=quantile(beta_hat_BOOT[,2], prob=c(0.025, 0.975)), col="blue")
hist(beta_hat_BOOT[,3], breaks=20, freq=FALSE, main="beta2",xlab=NA)
abline(v=-0.2826682,col="red")
abline(v=quantile(beta_hat_BOOT[,3], prob=c(0.025, 0.975)), col="blue")
hist(beta_hat_BOOT[,4], breaks=20, freq=FALSE, main="beta3",xlab=NA)
abline(v=-0.4057387,col="red")
abline(v=quantile(beta_hat_BOOT[,4], prob=c(0.025, 0.975)), col="blue")
```
#these plots look pretty normal for

#Creating a table of all of the samples that were taken via Bootstrap for loop
```{r code5}
beta_hat_BOOT
```



# Problem 4 [1 points]
Please knit this document to PDF (name should be `HW7_pid`) and push to BitBucket:

#In the R Terminal, type:  
#\begin{enumerate}
#    \item git pull  
#    \item git add HW7\_pid.[pR]*  (NOTE: this should add two files)  
#    \item git commit -m "final HW7 submission"   
#    \item git push
#\end{enumerate}



